tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr)* ;

expr returns [Integer out]
        : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = sub($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = mul($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
				| ^(MOD   e1=expr e2=expr) {$out = mod($e1.out, $e2.out);}
				| ^(POW   e1=expr e2=expr) {$out = pow($e1.out, $e2.out);}
				| ^(SQRT   e1=expr e2=expr) {$out = sqrt($e1.out, $e2.out);}
        | ^(PRINT e1=expr)         {print($e1.out.toString());}
        | ^(VAR name=ID)           {declareVar($name.text);}
        | (name=ID)                {$out = getVar($name.text);}
        | ^(PODST name=ID e1=expr) {setVar($name.text, $e1.out);}
        | INT                      {$out = getInt($INT.text);}
        | OB                       {localSymbols.enterScope();}
        | CB                       {localSymbols.leaveScope();}
        ;


catch [Exception ex]
{
	System.out.println(ex.getMessage());
}
