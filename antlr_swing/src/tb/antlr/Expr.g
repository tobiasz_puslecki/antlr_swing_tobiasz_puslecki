grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr -> ^(PRINT expr)
    | OB // open bracket
    | CB // close bracket
    | forLoop NL -> forLoop
    | NL ->
    ;


expr
    : mathExpr
      ( CMP^ mathExpr
      | NCMP^ mathExpr
      )*
    ;

mathExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powerExpr
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      )*
    ;

powerExpr
    : atom
      ( POW^ atom
      | SQRT^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

forLoop
    : FOR^ expr DO! expr
    ;

FOR :'for';

DO :'do';

CMP :'==';

CMP :'<>';

VAR :'var';

PRINT: 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

OB
	:	'{'
	;

CB
  :	'}'
  ;

LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

MOD
	:	'%'
	;

POW
	:	'^'
	;

SQRT
	:	'~'
	;
