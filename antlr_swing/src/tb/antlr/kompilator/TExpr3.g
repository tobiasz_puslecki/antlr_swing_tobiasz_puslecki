tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odd(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ^(PODST i1=ID   e2=expr) -> assign(key={$ID.text},value={$e2.st})
        | ^(CMP   e1=expr e2=expr) -> compare(p1={$e1.st},p2={$e2.st})
        | ^(NCMP  e1=expr e2=expr) -> nCompare(p1={$e1.st},p2={$e2.st})
        | ^(FOR    e1=expr e2=expr) {numer++;}  -> for(exp1={$e1.st},exp2={$e2.st},n={numer.toString()})
        | ^(DO     e1=expr e2=expr) {numer++;}  -> do(exp1={$e1.st},exp2={$e2.st},n={numer.toString()})
    ;
